# ISYS100 Assignment 1
## Kashi Samaraweera - S2, 2016

This repo contains the three-page website required for assignment 1 of ISYS100.
[git repo hosted on BitBucket](https://bitbucket.org/KashiMQ/isys100-assignment1)

## Colour reference

Colours used on this website:

 * Blue: **#01226B**
 * Light Blue: **#2A67AA**
 * Dark Blue: **#1E4370**
 * Beige: **#F1D4AD**
 * Maroon: **#D43E66**